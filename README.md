# Mastodon Toots Migration Queries Generator

## Warning

BEFORE DOING ANYTHING, BACKUP YOUR DATABASE!

## About

This is a quick and dirty migration tool. I used it to move from a multi-users instance to a mono-user one. I didn't want to start with an empty profile when I had quite a lot of toots into my previous instance.
I removed toots containing replies or mentions on purpose, because it would be strange to see conversations from an other location.

## Under the hood

- Fill in the `statuses`, `tags`, `statuses_tags`, `stream_entries` tables
- Replace the old instance references by the new one
- Generates from `outbox.json` a file containing all the queries needed that you can import into your psql database

## Requirements

- NodeJS > 6
- npm >= 5.6.0

## Install

$ npm install

## Usage

You may have to change the `user`, `table prefix` and `database` names for `psql` commands.

### Get the user id

`$ psql mastodon -c "SELECT id FROM users WHERE email = 'mail@domain.tld';"`

### Get the last status id

`$ psql mastodon -c "SELECT id FROM tags ORDER BY id desc LIMIT 1;"`

### Generate the queries file

1. Get the `outbox.json` file from your Mastodon's instance (`/settings/export`) and put it into this folder
2. `$ node index.js <userId> <oldInstanceUrl> <newInstanceUrl> <lastStatusesId>
Example: node index.js 2 oldinstance.social newinstance.social 10 > queries.sql`

### Import the queries intro your Mastodon database

`$ psql --host localhost --port 5432 --username mastodon_user mastodon_databases < queries.sql`


## Todo

1. get the last ID from the `statuses` table
2. better config checking

## Feel free to contribute

I know this is not perfect. If you have any idea to make it better, feel free to make a merge request to this repository.
