'use strict';
const beautify = require("json-beautify");
const strip = require('strip');
const htmlentities = require('htmlentities');
const fs = require('fs');
const path = require('path');
const filePath = path.join(__dirname, 'outbox.json');

const MastodonTootsMigrationQueriesGenerator = {
    config: {
        userId: 2,
        oldInstance: 'oldmastodoninstance.social',
        newInstance: 'newmastodoninstance.social',
        lastStatusesId: 10
    },

    setConfig: function () {
        const args = process.argv;
        args.shift();
        args.shift();
        const helpMessage = `Required arguments missing.\nCommand usage: node index.js <userId> <oldInstanceUrl> <newInstanceUrl> <lastStatusesId>\nExample: node index.js ${this.config.userId} ${this.config.oldInstance} ${this.config.newInstance} ${this.config.lastStatusesId}`;
        if(args.length !== 4) {
            console.log( helpMessage );
            return false;
        }
        this.config.userId = args[0];
        this.config.oldInstance = args[1];
        this.config.newInstance = args[2];
        this.config.lastStatusesId = args[3];
        return true;
    },

    getId: function (item) {
        return item.id.split('/')[6];
    },

    getDate: function (item) {
        return item.published
            .replace('T', ' ')
            .replace('Z', '.000');
    },

    getQueries: function (items) {
        if(!this.setConfig()) {
            return false;
        }
        const itemsWithoutReplies = items.filter(item => item.object.inReplyTo === null);
        this.getStatuses(itemsWithoutReplies);
        this.getTags(itemsWithoutReplies);
        this.getStreamEntries(itemsWithoutReplies);
    },

    getStatuses: function (items) {
        items.map( item => {
            const id = this.getId(item);
            const uri = item.id
                .replace(this.config.oldInstance, this.config.newInstance)
                .replace('/activity', '');
            const date = this.getDate(item);
            const str = htmlentities.decode(strip(item.object.content));
            const re = /\&apos;/gi;
            const text = str.replace(re, "''");
            const visibility = this.getStatusVisibility(item);
            const query = `INSERT INTO statuses (id, uri, text, created_at, updated_at, sensitive, visibility, local, account_id, application_id) VALUES ('${id}', '${uri}', '${text}', '${date}', '${date}', 'f', '${visibility}', 't', '${this.config.userId}', '1');`;
            console.log( query );
        });
    },

    getTags: function (items) {
        const datas = [];
        items.map( item => {
            const id = this.getId(item);
            item.object.tag.map(tag => {
                if(typeof(datas[tag.type]) === 'undefined') {
                    datas[tag.type] = [];
                }
                tag.id = id;
                tag['created'] = this.getDate(item);
                datas[tag.type].push(tag)
            });
        });
        const tagsSet = new Set();
        const tags = [];
        let tagId = this.config.lastStatusesId;
        const queries = [];
        const tagsToInsert = datas['Hashtag'].map(tag => {
            if(!tagsSet.has(tag.name)) {
                tagsSet.add(tag.name);
                tags[tag.name] = tagId;
                queries.push(`INSERT INTO tags (id, name, created_at, updated_at) VALUES ('${tagId}', '${tag.name.replace('#', '')}', '${tag.created}', '${tag.created}');`);
                ++tagId;
            }
            queries.push(`INSERT INTO statuses_tags (status_id, tag_id) VALUES ('${tag.id}', '${tags[tag.name]}');`);
        });
        queries.map(query => console.log(query));
    },

    getStreamEntries: function (items) {
        items.map( item => {
            const id = this.getId(item);
            const date = this.getDate(item);
            const hidden = this.isVisibilityFollowersOnly(item) ?
                't' : 'f';
            const query = `INSERT INTO stream_entries (activity_id, activity_type, created_at, updated_at, hidden, account_id) VALUES ('${id}', 'Status', '${date}', '${date}', '${hidden}', '${this.config.userId}');`;
            console.log( query );
        });
    },

    getStatusVisibility: function (item) {
        if (this.isVisibilityUnlisted(item)) {
            return 1;
        }
        if (this.isVisibilityFollowersOnly(item)) {
            return 2;
        }
        return 0;
    },

    isVisibilityUnlisted: function (item) {
        return item.to[0].endsWith('/followers') && item.cc[0] === 'https://www.w3.org/ns/activitystreams#Public';
    },

    isVisibilityFollowersOnly: function (item) {
        return item.to[0].endsWith('/followers') && item.cc.length === 0;
    }

}

if (fs.existsSync(filePath)) {
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
        const content = JSON.parse(data);
        process.argv.length > 2 && process.argv[2] === 'beautify' ?
            console.log( beautify(content, null, 2, 100) ) :
            MastodonTootsMigrationQueriesGenerator.getQueries(content.orderedItems);
    });
} else {
    console.log( `You must put your exported file from Mastodon at this location: ${filePath}` );
}
